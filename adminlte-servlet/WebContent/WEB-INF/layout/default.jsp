<%@page contentType="text/html; charset=UTF-8"%><%@ 
include file="/WEB-INF/include/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${page.applicationTitle }</title>
<tpl:htmlhead skin="purple" />
</head>
<body class="hold-transition <tpl:skin /> sidebar-mini">
	<div class="wrapper">
		<tpl:main-header />
		<tpl:sidebar />
		<tpl:content />
		<tpl:main-footer />
		<tpl:control-sidebar />
	</div>
	<tpl:javascripts />
</body>
</html>