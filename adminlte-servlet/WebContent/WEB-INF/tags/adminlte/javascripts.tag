<%@ tag language="java" pageEncoding="UTF-8" body-content="empty"%><%@ taglib
	prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- jQuery 3 -->
<script src="<c:url value="/ui/js/jquery.min.js"/>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<c:url value="/ui/js/bootstrap.min.js"/>"></script>
<!-- AdminLTE App -->
<script src="<c:url value="/ui/js/adminlte.min.js"/>"></script>
<script src="<c:url value="/ui/js/vue.min.js"/>"></script>
<script src="<c:url value="/res/js/app-init.js"/>"></script>
<!-- adminlte:script --><c:forEach items="${layoutScripts}" var="sc">
${sc}</c:forEach>
<!-- adminlte:js --><c:forEach items="${jsModules}" var="m">
<script src="${m}"></script></c:forEach>