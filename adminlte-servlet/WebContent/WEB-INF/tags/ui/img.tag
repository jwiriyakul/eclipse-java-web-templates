<%@ tag language="java" pageEncoding="UTF-8"%><%@
attribute name="src" type="java.lang.String" required="true" description="The image file name, append to lacation /res/images/"%><%@ 
attribute name="alt" type="java.lang.String"%><%@ 
attribute name="cssClass" type="java.lang.String"%><%@ 
taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><img src="<c:url value="/res/images/${src}"/>" alt="${alt}" class="${cssClass}" />