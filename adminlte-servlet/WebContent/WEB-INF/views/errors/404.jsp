<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ 
include file="/WEB-INF/include/taglibs.jsp"%>
<div class="error-page">
	<h2 class="headline text-yellow">404</h2>
	<div class="error-content">
		<h3>
			<i class="fa fa-warning text-yellow"></i> Oops! Page not found.
		</h3>

		<p>
			We could not find the page you were looking for. Meanwhile, you may <a
				href="${_mockPrefix}/">return to home page</a>
		</p>
	</div>
	<!-- /.error-content -->
</div>