<%@ page language="java" contentType="text/javascript; charset=utf-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
'use strict';
var jsContext = jsContext || {
	contextPath: '<c:url value="/"/>'.replace(/\/$/, ''),
	regExContextIncluded: new RegExp('^<c:url value="/"/>.*'),
	hasContext: function(path){
		return this.regExContextIncluded.text(path);
	},
	url: function(path){
		if(this.contexPath !== '' && !this.hasContext(path)){
			return this.contextPath + path;
		}
		return path;
	},
	lastMember: ''
};