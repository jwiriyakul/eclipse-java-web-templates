/**
 * initialize some functionality
 */
var jsContext = jsContext || {
	contextPath : '/'
};
$(function() {
	console.log("app initalized" + jsContext.contextPath);
	if(/.*;jsessionid.*/.test(window.location.href)){
		window.location.replace(window.location.href.replace(/;jsessionid.*$/,''));
	}
	// prepend context if not
	$.fn.resolveUrl = function() {
		return this.each(function() {
			var actor = $(this), includeAttrs = ['href','action'];
			$.each(includeAttrs, function(){
				var url = actor.attr(this);
				if (url && url !== '') {
					actor.attr(this, jsContext.url(url));
					return false;
				}
			});
		});
	}

	$('a[href^="/"],form[action^="/"]').resolveUrl();
});