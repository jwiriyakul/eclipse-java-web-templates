package jwiriyakul.adminlte.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultView {

	private String viewName;

	public DefaultView(String viewName) {
		this.viewName = viewName;
	}

	public String getViewName() {
		return viewName;
	}

	public void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("_view", getViewPrefix() + viewName + getSuffix());
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/layout/default.jsp");
		dispatcher.include(request, response);
	}

	private String getViewPrefix() {
		return "/WEB-INF/views/";
	}

	private String getSuffix() {
		return ".jsp";
	}

}
