<%@ tag language="java" pageEncoding="UTF-8" body-content="empty"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><i class="${page.icon }"></i> ${page.title } <small>${page.subTitle}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="${page.icon }"></i> Level</a></li>
			<li class="active">Here</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content container-fluid">
		<c:catch var="pageException">
			<jsp:include page="${_view}" />
		</c:catch>
		<c:if test="${pageException !=null }">
			<c:set var="exception" value="${pageException}" scope="request" />
			<%
				if (((Exception) request.getAttribute("exception")).getMessage().matches("^File.*not found$")) {
			%>
			<jsp:include page="/WEB-INF/views/errors/404.jsp" />
			<%
				} else {
			%>
			<jsp:include page="/WEB-INF/views/errors/500.jsp" />
			<%
				}
			%>
		</c:if>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->