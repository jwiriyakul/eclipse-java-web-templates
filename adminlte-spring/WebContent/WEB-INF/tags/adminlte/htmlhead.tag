<%@ tag language="java" pageEncoding="UTF-8" body-content="empty"%><%@ 
taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ 
attribute name="skin" type="java.lang.String" description="The skin name located at '/ui/css/skins'."%>
<c:set var="lte_skin" value="skin-${skin}" scope="request" />
<link rel="stylesheet" href="<c:url value="/ui/css/bootstrap.min.css"/>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<c:url value="/ui/css/font-awesome.min.css" />">
<!-- Ionicons -->
<link rel="stylesheet" href="<c:url value="/ui/css/ionicons.min.css"/>">
<!-- Theme style -->
<link rel="stylesheet" href="<c:url value="/ui/css/AdminLTE.min.css"/>">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
<link rel="stylesheet" href="<c:url value="/ui/css/skins/${lte_skin}.min.css"/>">
<link rel="stylesheet" href="<c:url value="/ui/css/font-sarabun.css"/>">
<link rel="stylesheet" href="<c:url value="/ui/css/customize.css"/>">
<script type="text/javascript">
	var jsContext = jsContext || {
		contextPath : '<c:url value="/"/>'.replace(/\/$/, ''),
		regExContextIncluded : new RegExp('^<c:url value="/"/>.*'),
		hasContext : function(path) {
			return this.regExContextIncluded.test(path);
		},
		url : function(path) {
			if (this.contexPath !== '' && !this.hasContext(path)) {
				return this.contextPath + path;
			}
			return path;
		},
		lastMember : ''
	};
</script>
