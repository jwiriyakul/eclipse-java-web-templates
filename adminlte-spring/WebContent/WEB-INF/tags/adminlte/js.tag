<%@ tag import="java.util.ArrayList"%><%@ 
tag language="java" pageEncoding="UTF-8" body-content="empty" description="Inserts script tag from location /res/js/modules/file"%><%@
taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ 
attribute name="file" required="true" type="java.lang.String"%><%@ 
attribute name="location" type="java.lang.String" description="The path relative to context path, specific the location of js file."%>
<%
	@SuppressWarnings("unchecked")
	ArrayList<String> jsModules = (ArrayList<String>) request.getAttribute("jsModules");
	if (jsModules == null) {
		jsModules = new ArrayList<>();
		request.setAttribute("jsModules", jsModules);
	}
	String jsPath = null;
	if (jspContext.getAttribute("location") != null) {
		jsPath = request.getContextPath() + ("/" + location + "/").replaceAll("//", "/") + jspContext.getAttribute("file");
	} else {
		jsPath = request.getContextPath() + "/res/js/" + jspContext.getAttribute("file");
	}
	jsModules.add(jsPath);
%>