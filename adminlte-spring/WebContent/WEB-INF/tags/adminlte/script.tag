<%@tag import="java.io.StringWriter"%><%@tag import="java.util.ArrayList"%><%@tag pageEncoding="UTF-8" body-content="scriptless" description="Write script and print on other location, best is on bottom of page by adminlte:javascripts tag."%>
<%
	@SuppressWarnings("unchecked")
	ArrayList<String> scripts = (ArrayList<String>) request.getAttribute("layoutScripts");
	if (scripts == null) {
		scripts = new ArrayList<>();
		request.setAttribute("layoutScripts", scripts);
	}
	StringWriter script = new StringWriter();
	getJspBody().invoke(script);
	scripts.add(script.toString());
%>