<%@ page language="java" pageEncoding="UTF-8"%><%@include
	file="/WEB-INF/include/taglibs.jsp"%>
<div class="error-page">
	<h2 class="headline text-red">403</h2>
	<div class="error-content">
		<h3>
			<i class="fa fa-warning text-red"></i> Oops! Permission denied.
		</h3>

		<p>
			You don't have permission to access this page, you may <a
				href="${_mockPrefix}/">return to home page</a>
		</p>
	</div>
	<!-- /.error-content -->
</div>