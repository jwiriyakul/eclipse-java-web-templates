<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ 
include file="/WEB-INF/include/taglibs.jsp"%>
<div class="error-page">
	<h2 class="headline text-red">500</h2>

	<div class="error-content">
		<h3>
			<i class="fa fa-warning text-red"></i> Oops! Something went wrong.
		</h3>

		<p>
			We will work on fixing that right away. Meanwhile, you may <a
				href="${_mockPrefix}/">return to home page</a>
		</p>
	</div>
</div>