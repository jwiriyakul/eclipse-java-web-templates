package jwiriyakul.adminlte.springmvc;

import org.springframework.web.bind.annotation.ModelAttribute;

public abstract class BaseController {

	@ModelAttribute("page")
	public WebPage getWebPage() {
		return new WebPage();
	}
}