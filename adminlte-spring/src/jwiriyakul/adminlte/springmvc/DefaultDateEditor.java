package jwiriyakul.adminlte.springmvc;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.util.StringUtils;

public class DefaultDateEditor extends PropertyEditorSupport {
	private static final ThreadLocal<DateFormat> DATE_FORMAT = new ThreadLocal<DateFormat>() {
		protected DateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
		};
	};
	private static final ThreadLocal<DateFormat> SHORT_DATE_FORMAT = new ThreadLocal<DateFormat>() {
		protected DateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		};
	};
	private final boolean allowEmpty;

	public DefaultDateEditor(boolean allowEmpty) {
		this.allowEmpty = allowEmpty;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (this.allowEmpty && !StringUtils.hasText(text)) {
			// Treat empty String as null value.
			setValue(null);
		} else {
			try {
				setValue(DefaultDateEditor.DATE_FORMAT.get().parse(text));
			} catch (ParseException ex) {
				try {
					setValue(DefaultDateEditor.SHORT_DATE_FORMAT.get().parse(text));
				} catch (ParseException ex2) {
					throw new IllegalArgumentException("Could not parse date: " + ex.getMessage(), ex);
				}
			}
		}
	}

	/**
	 * Format the Date as String, using the specified DateFormat.
	 */
	@Override
	public String getAsText() {
		Date value = (Date) getValue();
		String dateText = value != null ? DefaultDateEditor.DATE_FORMAT.get().format(value) : "";
		if (dateText != null && dateText.endsWith("00:00:00")) {
			dateText = dateText.substring(0, 10);
		}
		return dateText;
	}

}
