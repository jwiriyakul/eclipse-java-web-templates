package jwiriyakul.adminlte.springmvc;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

public class JspTemplateViewResolver extends UrlBasedViewResolver {
	public static final String JSP_VIEW_KEY = "_view";
	public static final String LAYOUT_PREFIX_DEFAULT = "/WEB-INF/layout/";
	private static final String VIEW_PREFIX_DEFAULT = "/WEB-INF/views/";
	public static final String SUFFIX_JSP = ".jsp";

	private String defaultLayout = "default";
	private LayoutConfig layouts;
	private String viewPrefix = VIEW_PREFIX_DEFAULT;

	public JspTemplateViewResolver() {
		layouts = new LayoutConfig();
		init();
	}

	public JspTemplateViewResolver(LayoutConfig layouts) {
		super();
		this.layouts = layouts;
		init();
	}

	private void init() {
		setPrefix(LAYOUT_PREFIX_DEFAULT);
		setSuffix(SUFFIX_JSP);
		setViewClass(JstlView.class);
	}

	public void setDefaultLayout(String defaultLayout) {
		this.defaultLayout = defaultLayout;
	}

	@Override
	protected AbstractUrlBasedView buildView(String viewName) throws Exception {
		Map<String, Object> attributes = new HashMap<String, Object>();
		// default set viewName as template content.
		JspView view = layouts.getLayoutName(viewName, defaultLayout);
		attributes.put(JSP_VIEW_KEY, getViewPrefix() + view.getViewName() + getSuffix());
		setAttributesMap(attributes);
		return super.buildView(view.getLayoutName());
	}

	public void setViewPrefix(String viewPrefix) {
		this.viewPrefix = viewPrefix;
	}

	private String getViewPrefix() {
		return viewPrefix;
	}
}