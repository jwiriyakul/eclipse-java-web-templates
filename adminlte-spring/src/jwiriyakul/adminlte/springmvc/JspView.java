package jwiriyakul.adminlte.springmvc;

public class JspView {
	private String layoutName;
	private String viewName;

	public JspView() {
	}

	public JspView(String layoutName) {
		super();
		this.layoutName = layoutName;
	}

	public JspView(String layoutName, String viewName) {
		super();
		this.layoutName = layoutName;
		this.viewName = viewName;
	}

	public String getLayoutName() {
		return layoutName;
	}

	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

}
