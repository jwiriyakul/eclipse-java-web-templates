package jwiriyakul.adminlte.springmvc;

import java.util.LinkedHashMap;

/**
 * 
 * @author User
 *
 */
@SuppressWarnings("serial")
public class LayoutConfig extends LinkedHashMap<String, String> {

	public JspView getLayoutName(String viewName, String defaultLayout) {
		JspView view = new JspView(defaultLayout);
		for (String prefix : keySet()) {
			if (viewName.matches(prefix + ":.+")) {
				view.setLayoutName(get(prefix));
			}
		}
		view.setViewName(viewName.substring(viewName.indexOf(":") + 1));
		return view;
	}

}
