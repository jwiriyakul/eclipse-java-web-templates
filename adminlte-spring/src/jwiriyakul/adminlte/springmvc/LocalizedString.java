package jwiriyakul.adminlte.springmvc;

public class LocalizedString {
	private String code;
	private String defaultValue = "";

	public LocalizedString() {
	}

	public LocalizedString(String code, String defaultValue) {
		super();
		this.code = code;
		this.defaultValue = defaultValue;
	}

	public LocalizedString(String text) {
		this.defaultValue = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public String toString() {
		// TODO: format localization.
		return defaultValue;
	}
}
