package jwiriyakul.adminlte.springmvc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
//TODO: Add your spring's annotated packages here.
@ComponentScan(basePackages = { "jwiriyakul.adminlte.springmvc" })
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		LayoutConfig layouts = new LayoutConfig();
		layouts.put("js", "js");
		registry.viewResolver(new JspTemplateViewResolver(layouts));
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/errors/404").setViewName("errors/404");
		registry.addViewController("/errors/403").setViewName("errors/403");
		registry.addViewController("/errors/500").setViewName("errors/500");
		registry.addViewController("/scripts/js-context.js").setViewName("js:js/js-context");		
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// TODO: add resource handler here
		registry.addResourceHandler("/res/**").addResourceLocations("/res/");
		registry.addResourceHandler("/ui/**").addResourceLocations("/ui/");
	}
	
}
