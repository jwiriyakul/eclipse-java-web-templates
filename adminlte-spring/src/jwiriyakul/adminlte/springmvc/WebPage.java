package jwiriyakul.adminlte.springmvc;

public class WebPage {
	private static LocalizedString defaultApplicationTitle = new LocalizedString(null, "adminlte-spring");
	private LocalizedString applicationTitle = defaultApplicationTitle;
	private LocalizedString title;
	private LocalizedString subTitle;
	private String icon;

	public WebPage() {
	}

	public LocalizedString getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(LocalizedString applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public LocalizedString getTitle() {
		return title;
	}

	public void setTitle(LocalizedString title) {
		this.title = title;
	}

	public LocalizedString getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(LocalizedString subTitle) {
		this.subTitle = subTitle;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getIcon() {
		return icon;
	}

}
