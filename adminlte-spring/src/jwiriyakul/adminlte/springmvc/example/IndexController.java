package jwiriyakul.adminlte.springmvc.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jwiriyakul.adminlte.springmvc.BaseController;

@Controller
public class IndexController extends BaseController{

	@RequestMapping("/")
	public String showIndex() {
		return "index";
	}
}
